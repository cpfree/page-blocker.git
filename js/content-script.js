filterUrls = ['v.qq.com', 'www.bilibili.com', 'www.iqiyi.com', 'www.mgtv.com', 'www.youtube.com']

// 替换页面
function replacePage(content){
   document.write(content);
   window.stop();
}

chrome.storage.sync.get(["replaceAll","doOrDieHtml"], ({ replaceAll, doOrDieHtml }) => {
   // 如果拦截所有开启
   if (replaceAll) {
      replacePage(doOrDieHtml);
   } else {
      // 检测到视频网站, 则替换网页
      var url = window.location.href;
      var find = filterUrls.find(it => url.indexOf(it) >= 0);
      if (find) {
         replacePage(doOrDieHtml);
      }
   }
});

