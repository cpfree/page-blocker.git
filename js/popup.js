const btn = document.querySelector("#switch");
 
chrome.storage.sync.get("replaceAll", ({ replaceAll }) => {
  btn.checked = replaceAll;
});
 
btn.addEventListener("change", () => {
  if (btn.checked) {
    chrome.storage.sync.set({ replaceAll: true });
  } else {
    chrome.storage.sync.set({ replaceAll: false });
  }
  // 获取当前tab窗口
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
  });
});
 
// 刷新页面
function refreshPage() {
  window.location.reload();
}
