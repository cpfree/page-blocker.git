// 用户首次安装插件时执行一次，后面不会再重新执行。(除非用户重新安装插件)
chrome.runtime.onInstalled.addListener(() => {
   // 插件功能安装默认启用  
   chrome.storage.sync.set({
     replaceAll: false,
     doOrDieHtml: '<!DOCTYPE HTML><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8;"/><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><meta name="robots" content="all" /><meta name="robots" content="index,follow"/><style>            @import url(https://fonts.googleapis.com/css?family=Open+Sans:800);      .text {      fill: none;      stroke-width: 5;      stroke-dasharray: 0 300;      stroke-dashoffset: 0;      }      .text:nth-child(3n + 1) {      stroke: #FFFFFF;         animation: stroke 10s  ease-in-out forwards;        }      .text:nth-child(3n + 2) {      stroke: #808080;      animation: stroke1 10s  ease-in-out forwards;      }      .text:nth-child(3n + 3) {      stroke: #696969;      animation: stroke2 10s  ease-in-out forwards;      }      @keyframes stroke {      100% {         stroke-dashoffset: 1000;         stroke-dasharray: 80 160;      }      }      @keyframes stroke1 {      100% {         stroke-dashoffset: 1080;         stroke-dasharray: 80 160;      }      }      @keyframes stroke2 {      100% {         stroke-dashoffset: 1160;         stroke-dasharray: 80 160;      }      }            html, body {      height: 100%;      }      body {      background: #000000;      background-size: .2em 100%;      font: 12.5em/1 Open Sans, Impact;      text-transform: uppercase;      margin: 0;      }      svg {      position: absolute;      width: 100%;      height: 100%;      }      canvas{         display: block;      }      body {         background:#0a0a0a;         overflow:hidden;      }</style></head><body><svg viewBox="-20 0 1450 600"><symbol id="s-text"><text text-anchor="middle" x="50%" y="25%" dy=".25em">         人生时间不多</text><text text-anchor="middle" x="50%" y="75%" dy=".25em">         DO OR DIE</text></symbol><use xlink:href="#s-text" class="text"       ></use><use xlink:href="#s-text" class="text"       ></use><use xlink:href="#s-text" class="text"       ></use></svg></body></html>'
   });
 });

 // 监听tab页面加载状态，添加处理事件
 chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
   // 设置判断条件，页面加载完成才添加事件，否则会导致事件重复添加触发多次
   if (changeInfo.status === "complete" && /^http/.test(tab.url)) {
   }
 });